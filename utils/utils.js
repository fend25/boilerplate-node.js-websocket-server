'use strict';

const log = require('log4js').getLogger('utils');

exports.delay = function* delay(ms) {
  return new Promise(function(resolve){
    setTimeout(resolve, ms);
  })
};

exports.parseAndValidateInteger = function(passedNum) {
  const num = parseInt(passedNum);
  if (isNaN(num) || num < 0) return null;
  return num;
};

exports.prettifyErrors = function(errors) {
  if (errors === null) return null;
  return errors.map(e => `cmd${e.dataPath} ${e.message}`);
};