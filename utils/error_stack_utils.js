'use strict';

/**
 * returns new Error with provided name and message
 * @param {string} name
 * @param {string} msg
 * @returns {Error}
 */
function errByNameAndMsg(name, msg) {
  const err = new Error(msg);
  err.name = name;
  return err;
}
exports.errByNameAndMsg = errByNameAndMsg;

/**
 * returns new Error with provided name and message, and extra field 'info'
 * @param {string} name
 * @param {string} msg
 * @param {Object} info
 * @returns {Error}
 */
function errByNameAndMsgAndInfo(name, msg, info) {
  const err = new Error(msg);
  err.name = name;
  err.info = info;
  return err;
}
exports.errByNameAndMsgAndInfo = errByNameAndMsgAndInfo;

exports.newUnimplementedError = function(info) {
  return errByNameAndMsgAndInfo('Unimplemented', 'unimplemented', info);
};

exports.newInvalidFieldError = function(info) {
  return errByNameAndMsgAndInfo('InvalidField', 'invalid field', info);
};

exports.newInternalServerError = function(msg, info) {
  return errByNameAndMsgAndInfo('InternalServerError', msg, info);
};

/**
 * returns error CustomError with provided code, severity, msg and info
 * @param {int} errCode
 * @param {int} errSeverity
 * @param {=string} msg
 * @param {=Object} info
 * @returns {Error}
 */
exports.newCustomError = function(errCode, errSeverity, msg, info) {
  const err = errByNameAndMsgAndInfo('CustomError', msg, info);
  err.errCode = errCode;
  err.errSeverity = errSeverity;
  return err;
};