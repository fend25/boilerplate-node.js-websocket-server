'use strict';

const log = require('log4js').getLogger('main');
log.info('App started');

const db = require('./db.js');

require('./ws/server.js');

process.on('uncaughtException', function(e) {
  log.error('uncaughtException', e.toString());
});