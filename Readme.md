# Sparklabs WebSocket Server
Boilerplate Node.js WebSocket Server with auth and commands validation. Based on generators and synchronous-style async code. DBMS is PostgreSQL.

## Install
```
npm install
cp config/_default_configs/* config/
# dont forget to view and edit all configs.
```

## Start
```
npm start
```
or with nodemon
```
npm run nodemon
```

## Requirements
 - node.js@latest (at least 5.x.x)
 - PostgreSQL@9.4 (highly recommended latest, now it's 9.5)

## Documentation
Now in progress.
Basic commands schema is described in the `Commands.md`. There are examples too.

## Tests
Now in progress.

## License
MIT