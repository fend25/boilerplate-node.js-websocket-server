'use strict';

module.exports = {
  host: 'localhost',
  port: 5432,
  database: 'db',
  user: 'user',
  password: 'password',
  ssl: true
};