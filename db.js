'use strict';

const log = require('log4js').getLogger('db');

const pgp = require('pg-promise')();
const config = require('./config/db_config.js');
const db = pgp(config);

const connParamsForLog = `${config.host}:${config.port}/${config.database}, ` +
 `user "${config.user}", ssl ${config.ssl ? 'on' : 'off'}`;

db.connect()
  .then((result) => {
    log.info('successfully connected to the db', connParamsForLog);
    result.done();
  })
  .catch((err) => {
    log.fatal(err.toString(), 'connection params:', connParamsForLog);
    process.exit();
  });

module.exports = db;