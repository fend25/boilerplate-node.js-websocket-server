#Commands

## auth
### AUTH_WITH_LOGIN_PASSWORD: 10
example:
```json
{
  "code": 10,
  "email": "",
  "password": "",
  "cookie": true,
  "user_agent": {
    "name": "wscat",
    "version": "1.0.1",
    "platform": "win32",
    "os_version":"Windows NT 6.1"
  }
}
```

### AUTH_WITH_COOKIE: 11
example:
```json
{
  "code": 11,
  "cookie": {
    "name": "sess_id",
    "value": "eac5e2e9225390de33e3de84e7fa4898"
  },
  "user_agent": {
    "name": "wscat",
    "version": "1.0.1",
    "platform": "win32",
    "os_version":"Windows NT 6.1"
  }
}
```

### LOGOUT: 20
example:
```json
{"code": 20}
```