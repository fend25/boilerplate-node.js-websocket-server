'use strict';

const log = require('log4js').getLogger('ws');
const co = require('co');

const constants = require('../constants.js');
const wsCodes = constants.WS_COMMAND_CODES;
const errCodes = constants.WS_ERROR_CODES;
const errSeverities = constants.WS_SEVERITY_LEVELS;

const utils = require('../utils/utils.js');
const errorStackUtils = require('../utils/error_stack_utils.js');

const wsLib = require('ws');
const Server = wsLib.Server;
const config = require('../config/wsserver_config.js');

const server = new Server(config);

let maxWsId = 0;
const getNextWsId = () => maxWsId++;
//const clients = server.clients;

server.on('connection', function onConnection(ws) {
  ws.id = getNextWsId();
  ws.user = null;
  log.debug('connected', ws.protocol, ws.id);

  ws.on('message', function onMessage(data, flags) {
    co(messageReceived(ws, data, flags));
  });

  ws.on('error', function onError(msg) {
    log.warn('error', ws.protocol, ws.id, 'reason:', msg);
  });
  ws.on('close', function onClose(msg) {
    log.debug('closed', ws.protocol, ws.id, 'reason:', msg);
  });


  ws.sendSync = function* (data, flags) {
    if (ws.readyState !== wsLib.OPEN) {
      log.warn('trying to send through closed socket, current readyState:', ws.readyState);
      return;
    }
    return new Promise(function(resolve) {
      ws.send(data, flags, resolve);
    });
  };
  ws.respondJson = function* (object) {
    return yield ws.sendSync(JSON.stringify(object));
  };
  ws.respondBinary = function* (buffer) {
    return yield ws.sendSync(buffer, {binary: true});
  };
  /**
   * Sends error message to the client
   * @param {number} error - error code
   * @param {number} severity - severity level
   * @param {Object} cmd - command (or raw json if parse() failed) on which handling error has occurred
   * @param {string} msg - error message. may be and object
   * @param {=Object} info - additinal info object
   * @returns {*}
   */
  ws.respondError = function* (error, severity, cmd, msg, info) {
    return yield ws.sendSync(JSON.stringify({error, severity, cmd, msg, info}));
  };
});

//require all handlers
let handlers = [];
require('./handlers/auth_handlers.js')(handlers);
//log.info(handlers.filter(h => typeof h !== 'undefined'));

//invert command codes object to command names array
let commandNames = [];
for (let i in wsCodes) commandNames[wsCodes[i]] = i;

function* messageReceived(ws, data, flags) {
  let cmd = null;
  try {
    cmd = JSON.parse(data);
    log.trace('msg', ws.protocol, ws.id, commandNames[cmd.code], data.slice(0,50));

    yield validateCommand(ws, cmd);

    //delay for cmd.delay milliseconds, if such field is provided
    if (cmd.hasOwnProperty('delay')) {
      const delay = utils.parseAndValidateInteger(cmd.delay);
      if (delay) yield utils.delay(delay);
    }

    //process command
    yield handlers[cmd.code](ws, cmd);
  } catch (e) {
    yield onException(ws, cmd, data, flags, e);
  }
}

function* validateCommand(ws, cmd) {
  //check whether the code field exists and is number
  if (typeof cmd.code !== 'number') {
    const err = errorStackUtils.newInvalidFieldError({code: cmd.code});
    err.message += ' code';
    throw err;
  }

  //check whether the function exists for the provided command code
  if (typeof handlers[cmd.code] !== 'function') {
    throw errorStackUtils.errByNameAndMsgAndInfo(
      'CommandNotFound',
      'command code not found',
      {code: cmd.code}
    );
  }

  //don't pass unauthed client for auth-required commands
  if (ws.user === null && cmd.code >= constants.AUTH_REQUIRED_FOR_COMMAND_CODE_FROM) {
    throw errorStackUtils.errByNameAndMsg(
      'Unauthorized',
      `client tries to execute command ${cmd.code}-${commandNames[cmd.code]} which requires auth`
    );
  }
}

function* onException(ws, cmd, data, flags, e) {
  let errCode = 0;
  let severity = errSeverities.ERROR;

  let unhandledError = false;
  switch (e.name) {
    case 'SyntaxError':
      errCode = errCodes.INVALID_JSON;
      if (!cmd) cmd = data;
      e.message = e.toString();
      break;
    case 'InvalidField':
      errCode = errCodes.INVALID_FIELD;
      break;
    case 'CommandNotFound':
      errCode = errCodes.COMMAND_NOT_FOUND;
      break;
    case 'Unauthorized':
      errCode = errCodes.UNAUTHORIZED;
      break;
    case 'Unimplemented':
      errCode = errCodes.UNIMPLEMENTED;
      severity = errSeverities.WARN;
      break;
    case 'CustomError':
      errCode = e.errCode;
      severity = e.errSeverity;
      break;
    case 'error':
    case 'InternalServerError':
      errCode = errCodes.INTERNAL_SERVER_ERROR;
      severity = errSeverities.WARN;

      log.warn(e.stack);
      break;
    default:
      unhandledError = true;
      errCode = errCodes.INTERNAL_SERVER_ERROR;
      severity = errSeverities.WARN;
      break;
  }


  //if its common, unspecified error, process it as error, not normal situation
  if (unhandledError) {
    log.error('error server', ws.protocol, ws.id, e.name, e.message, e.stack);
  } else {
    log.debug('error client', ws.protocol, ws.id, e.name, e.message);
  }
  yield ws.respondError(errCode, severity, cmd, e.message, e.info);
}