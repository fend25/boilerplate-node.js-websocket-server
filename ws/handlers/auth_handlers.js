'use strict';

const log = require('log4js').getLogger('handlers');

const constants = require('../../constants.js');
const wsCodes = constants.WS_COMMAND_CODES;
const errCodes = constants.WS_ERROR_CODES;
const errSeverities = constants.WS_SEVERITY_LEVELS;

const utils = require('../../utils/utils.js');
const prettifyErrors = utils.prettifyErrors;

const errors = require('../../utils/error_stack_utils.js');

const Ajv = require('ajv');
const ajv = Ajv({allErrors: true});

const db = require('../../db.js');

const mz = require('mz');
const crypto = mz.crypto;

const pwdSalt = require('../../config/salts.js').pwdSalt;
function* sha256(password) {
  return (yield crypto.pbkdf2(password, pwdSalt, 10000, 256, 'sha256')).toString('hex');
}
function md5(str) {
  return crypto.createHash('md5').update(str).digest('hex');
}

let handlers = [];
//merge all handlers with this file handlers
module.exports = function(allHandlers) {
  handlers.forEach((handler, index) => allHandlers[index] = handler);
};

const userAgentSchema = {
  type: 'object',
  properties: {
    name: {type: 'string', minLength: 1},
    version: {type: 'string', minLength: 1},
    platform: {type: 'string', minLength: 1},
    os_version: {type: 'string', minLength: 1}
  },
  required: ['name', 'version', 'platform', 'os_version']
};

/**
 * adds a record to the login_stat table with user's user_agent data
 * @param ws {WebSocket}
 * @param person_id {int}
 * @param cookie_id {int}
 * @param ua {Object}
 * @returns {*}
 */
function* fillLoginStat(ws, person_id, cookie_id, ua) {
  const query = `INSERT INTO login_stat (
    person_id, cookie_id, ua_name, ua_version, ua_platform, ua_os_version,
    ip_address
  ) VALUES (
    $(person_id), $(cookie_id), $(ua_name), $(ua_version), $(ua_platform), $(ua_os_version),
    $(ip_address)
  ) RETURNING id, ts`;
  return yield db.oneOrNone(query, {
    person_id,
    cookie_id,
    ua_name: ua.name,
    ua_version: ua.version,
    ua_platform: ua.platform,
    ua_os_version: ua.os_version,
    ip_address: ws.upgradeReq.connection.remoteAddress
  });
}

//================================
// authWithLoginPassword
//================================
const authWithLoginPasswordValidator = ajv.compile({
  properties: {
    email: {type: 'string', format: 'email'},
    password: {type: 'string', minLength: 3},
    cookie: {type: 'boolean'},
    user_agent: userAgentSchema
  },
  required: ['email', 'password', 'user_agent']
});
//const cmd = {code: 10, email: 'a@a.a', password: 'abc', user_agent: {name: 'a', version: 'b', platform: 'c', os_version: 'd'}};
//log.debug(authWithLoginPasswordValidator(cmd), prettifyErrors(authWithLoginPasswordValidator.errors));
handlers[wsCodes.AUTH_WITH_LOGIN_PASSWORD] = function* authWithLoginPassword(ws, cmd) {
  const validator = authWithLoginPasswordValidator;
  if (!validator(cmd)) throw errors.newInvalidFieldError(prettifyErrors(validator.errors));

  //get id, password and role data by email
  let query =
    `SELECT p.id, p.password, r.id AS role_id, r.name AS role_name, r.permission AS role_permission
    FROM person p
    JOIN "role" r ON p.role_id = r.id
    WHERE p.email = $(email)
    `;
  const user = yield db.oneOrNone(query, {email: cmd.email});
  if (user === null) //if no person with provided email found
    throw errors.newCustomError(errCodes.USER_NOT_FOUND, errSeverities.WARN, 'user not found');

  //check password
  const hashedPassword = yield sha256(cmd.password);
  if (user.password !== hashedPassword)
    throw errors.newCustomError(errCodes.PASSWORD_DOESNT_MATCH, errSeverities.WARN, 'password doesnt match');

  //authorize user: fill the ws.user field with user id and role data
  ws.user = {
    id: user.id,
    role: {
      id: user.role_id,
      name: user.role_name,
      permission: user.role_permission
    }
  };

  //create and set cookie, if requested (cmd.cookie === true)
  let cookieId = null; //we'll need null or value further in fillLoginStat
  if (cmd.cookie) {
    const cookieName = 'sess_id';
    const cookieValue = md5(cmd.email + Date.now().toString() + (Math.random()*1000000000).toString());

    query = `INSERT INTO cookie (name, value, person_id) VALUES
      ($(cookieName), $(cookieValue), $(person_id)) RETURNING id`;
    const cookie = yield db.oneOrNone(query, {cookieName, cookieValue, person_id: user.id});
    if (!cookie)
      throw errors.newInternalServerError('authWithLoginPassword empty cookie', {cookie});

    cookieId = cookie.id;
    ws.user.cookie = {name: cookieName, value: cookieValue};
  }

  //remember login fact: add record to the login_stat table
  yield fillLoginStat(ws, user.id, cookieId, cmd.user_agent);

  //respond to client
  return yield ws.respondJson(ws.user);
};


//================================
// authWithLoginCookie
//================================
const authWithCookieValidator = ajv.compile({
  properties: {
    cookie: {
      name: {type: 'string'},
      value: {type: 'string', minLength: 40, maxLength: 40}
    },
    user_agent: userAgentSchema
  },
  required: ['cookie', 'user_agent']
});
handlers[wsCodes.AUTH_WITH_COOKIE] = function* authWithCookie(ws, cmd) {
  //throw errors.newUnimplementedError();

  const validator = authWithCookieValidator;
  if (!validator(cmd)) throw errors.newInvalidFieldError(prettifyErrors(validator.errors));

  let query =
    `SELECT p.id, c.id AS cookie_id, p.role_id, r.id AS role_id, r.name AS role_name, r.permission AS role_permission
    FROM cookie c
    JOIN person p ON c.person_id = p.id
    JOIN "role" r ON p.role_id = r.id
    WHERE c.name = $(name) AND c.value = $(value)
    `;
  const user = yield db.oneOrNone(query, cmd.cookie);
  if (user === null) //if no person with provided email found
    throw errors.newCustomError(errCodes.COOKIE_NOT_FOUND, errSeverities.WARN, 'user with provided cookie not found');

  //authorize user: fill the ws.user field with user id and role data
  ws.user = {
    id: user.id,
    cookie: cmd.cookie,
    role: {
      id: user.role_id,
      name: user.role_name,
      permission: user.role_permission
    }
  };

  //remember login fact: add record to the login_stat table
  yield fillLoginStat(ws, user.id, user.cookie_id, cmd.user_agent);

  //respond to client
  return yield ws.respondJson(ws.user);
};

handlers[wsCodes.LOGOUT] = function* logout(ws, cmd) {
  //throw errors.newUnimplementedError();

  if (ws.user.cookie) {
    let query = `DELETE FROM cookie WHERE "name" = $(name) AND value = $(value)`;
    yield db.oneOrNone(query, ws.user.cookie);
  }

  ws.user = null;
  return yield ws.respondJson({ok: true});
};